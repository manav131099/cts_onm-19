rm(list = ls())

rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}

nodays = function(y,x)
{
  x = as.numeric(x)
  y = as.numeric(y)
 {
    
  if(x == 1 || x == 3 || x == 5 || x == 7 || x == 8 || x == 10 || x == 12)
  {
    return(31)
  }
  else if(x == 4 || x == 6 || x == 9 || x == 11)
  {
    return(30)
  }
  else if(x == 2 && y %% 4 == 0 )
  {
    return(29)
  }
  else
  {
    return(28)
  }
}
}
monthlyirr1 = 0
monthlyirr2 = 0
monthlyirr3 = 0
monthlyirr4 = 0
dayssofarmonth = 0
daystruemonth = 0
last30days = last30days2 = vector('numeric',30)
last30daysidx = 1
dropsoil = 0
dropsnp = 0
daysactive = 0
globirr1 = 0
globirr2 = 0
globirr3 = 0
globirr4 = 0
dobstation = "5 Jun 2016"

path = "/home/admin/Dropbox/Second Gen/[KH-714S]"
years = dir(path)
for(x in 1 : length(years))
{
 pathmonth = paste(path,years[x],sep="/")
 months = dir(pathmonth)
 for(y in 1 : length(months))
 {
   monthlyirr1 = monthlyirr2 = monthlyirr3 = monthlyirr4 = 0 
   pathdays = paste(pathmonth,months[y],sep="/")
   strng = unlist(strsplit(months[y],"-"))
   daystruemonth = nodays(strng[1],strng[2])
   dayssofarmonth = 0
   days = dir(pathdays)
   sumfilename = paste("[KH-714S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
   for(z in 1 : length(days))
   {
     if(days[z] == sumfilename)
     {
       next
     }
     dataread = read.table(paste(pathdays,days[z],sep="/"),header = T,sep="\t")
     monthlyirr1 = monthlyirr1 + as.numeric(dataread[1,3])
     monthlyirr2 = monthlyirr2 + as.numeric(dataread[1,4])
     monthlyirr3 = monthlyirr3 + as.numeric(dataread[1,5])
		 monthlyirr4 = monthlyirr4 + as.numeric(dataread[1,6])
     globirr1 = globirr1 + as.numeric(dataread[1,3])
     globirr2 = globirr2 + as.numeric(dataread[1,4])
     globirr3 = globirr3 + as.numeric(dataread[1,5])
		 globirr4 = globirr4 + as.numeric(dataread[1,6])
     dayssofarmonth = dayssofarmonth + 1    
     daysactive = daysactive + 1
		 print(paste(days[z],"is",daysactive))
		 last30days[[last30daysidx]] = as.numeric(dataread[1,3])
     last30days2[[last30daysidx]] = as.numeric(dataread[1,6])
     last30daysidx = (last30daysidx +1 )%% 31
     if(last30daysidx == 0) {last30daysidx =1}
   }
 }
}

LAST30DAYSTOT = sum(last30days)
LAST30DAYSMEAN = mean(last30days)
LAST30DAYSTOT2 = sum(last30days2)
LAST30DAYSMEAN2 = mean(last30days2)
DAYSACTIVE = daysactive
MONTHLYIRR1 = monthlyirr1
MONTHLYIRR2 = monthlyirr2
MONTHLYIRR3 = monthlyirr3
MONTHLYIRR4 = monthlyirr4
MONTHAVG1 = monthlyirr1 / dayssofarmonth
MONTHAVG2 = monthlyirr2 / dayssofarmonth
MONTHAVG3 = monthlyirr3 / dayssofarmonth
MONTHAVG4 = monthlyirr4 / dayssofarmonth
FORECASTIRR1 = MONTHAVG1 * daystruemonth
FORECASTIRR2 = MONTHAVG2 * daystruemonth
FORECASTIRR3 = MONTHAVG3 * daystruemonth
FORECASTIRR4 = MONTHAVG4 * daystruemonth
SOILINGDEC = (globirr2 / globirr1) - 1
SOILINGDEC2 = (globirr3/globirr1) - 1
SNPDEC = (globirr4 / globirr1) - 1
SOILINGDECPD = SOILINGDEC / DAYSACTIVE
SOILINGDECPD2 = SOILINGDEC2/DAYSACTIVE
SNPDECPD = SNPDEC / DAYSACTIVE

LAST30DAYSTOT=rf(LAST30DAYSTOT) 
LAST30DAYSMEAN=rf(LAST30DAYSMEAN) 
LAST30DAYSTOT2=rf(LAST30DAYSTOT2) 
LAST30DAYSMEAN2=rf(LAST30DAYSMEAN2)
DAYSACTIVE=rf(DAYSACTIVE) 
MONTHLYIRR1=rf(MONTHLYIRR1)
MONTHLYIRR2=rf(MONTHLYIRR2)
MONTHLYIRR3=rf(MONTHLYIRR3)
MONTHLYIRR4=rf(MONTHLYIRR4)
MONTHAVG1=rf(MONTHAVG1)
MONTHAVG2=rf(MONTHAVG2)
MONTHAVG3=rf(MONTHAVG3) 
MONTHAVG4=rf(MONTHAVG4)
FORECASTIRR1=rf(FORECASTIRR1) 
FORECASTIRR2=rf(FORECASTIRR2) 
FORECASTIRR3=rf(FORECASTIRR3) 
FORECASTIRR4=rf(FORECASTIRR4)
SOILINGDEC=rf(SOILINGDEC) 
SOILINGDEC2=rf(SOILINGDEC2)
SNPDEC=rf(SNPDEC)
SOILINGDECPD=SOILINGDECPD
SOILINGDECPD2=SOILINGDECPD2
SNPDECPD=SNPDECPD
