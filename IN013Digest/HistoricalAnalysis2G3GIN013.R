system('rm -R "/home/admin/Dropbox/Second Gen/[IN-013X]"')
system('rm -R "/home/admin/Dropbox/Third Gen/[IN-013X]"')

require('mailR')
require('Hmisc')

source('/home/admin/CODE/IN013Digest/IN013MailDigestFunctions.R')

path = '/home/admin/Dropbox/Gen 1 Data/[IN-013X]'
writepath2G = '/home/admin/Dropbox/Second Gen/[IN-013X]'
checkdir(writepath2G)

writepath3G = '/home/admin/Dropbox/Third Gen/[IN-013X]'
checkdir(writepath3G)
DAYSALIVE = 0
#LAST30DAYSEAC1 = vector('numeric',30)
#LAST30DAYSEAC2 = vector('numeric',30)
#MONTHTOTEAC1 = 0
#MONTHTOTEAC2 = 0
#DAYSTHISMONTH = 0
#DAYSTHISMONTHAC = 0
#EAC1GLOB = 0
#EAC2GLOB = 0
idxvec = 1
years = dir(path)
for(x in 1 : length(years))
{
  pathyrs = paste(path,years[x],sep="/")
  writepath2Gyr = paste(writepath2G,years[x],sep="/")
  checkdir(writepath2Gyr)
  writepath3Gyr = paste(writepath3G,years[x],sep="/")
  checkdir(writepath3Gyr)
  months = dir(pathyrs)
  for(y in 1 : length(months))
  {
    MONTHTOTEAC1 = 0
    MONTHTOTEAC2 = 0
    DAYSTHISMONTH = 0
    pathstations= paste(pathyrs,months[y],sep="/")
		stationsread = dir(pathstations)
    writepath2Gstations = paste(writepath2Gyr,months[y],sep="/")
    checkdir(writepath2Gstations)
		pathdays1 = paste(pathstations,stationsread[1],sep="/")
		writepath2Gdays1 = paste(writepath2Gstations,stationsread[1],sep="/")
    checkdir(writepath2Gdays1)
    days = dir(pathdays1)
		if(length(stationsread)>1){
		pathdays2 = paste(pathstations,stationsread[2],sep="/")
		writepath2Gdays2 = paste(writepath2Gstations,stationsread[2],sep="/")
    checkdir(writepath2Gdays2)
		days2 = dir(pathdays2)
		lenuse = min(length(days),length(days2))
		days = tail(days,n=lenuse)
		days2 = tail(days2,n=lenuse)
		}
    writepath3Gfinal = paste(writepath3Gyr,"/[IN-013X] ",months[y],".txt",sep="")
    temp = unlist(strsplit(days[1]," "))
    temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
    #DAYSTHISMONTHAC = monthDays(temp)

      for(t in 1 : length(days))
      {
        {
          if(x == 1 && y == 1 && t == 1)
          {
            DOB = unlist(strsplit(days[t]," "))
            DOB = substr(DOB[3],1,10)
            DOB = as.Date(DOB,"%Y-%m-%d")
          }
          else if(x == length(years) && y == length(months) && t == length(days))
          {
            next
          }
        }
        DAYSALIVE = DAYSALIVE + 1
        writepath2Gfinal = paste(writepath2Gdays1,"/",days[t],sep="")
        readpath = paste(pathdays1,days[t],sep="/")
        df1 = secondGenData(readpath,writepath2Gfinal,1)
        {
				if(length(stationsread) > 1){
				writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
        readpath2 = paste(pathdays2,days2[t],sep="/")
        df2= secondGenData(readpath2,writepath2Gfinal2,2)
        tots = thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath3Gfinal)}
				else
        	tots = thirdGenData(writepath2Gfinal,NULL,writepath3Gfinal)
				}
				if(!is.finite(tots[1])) 
				{
					tots[1]=0
				}
				if(!is.finite(tots[2]))
				{
					tots[2]=0
				}
        #LAST30DAYSEAC1[[idxvec]] = tots[1]
        #LAST30DAYSEAC2[[idxvec]] = tots[2]
        #MONTHTOTEAC1 = MONTHTOTEAC1 + tots[1]
        #MONTHTOTEAC2 = MONTHTOTEAC2 + tots[2]
        #DAYSTHISMONTH = DAYSTHISMONTH + 1
        #EAC1GLOB = EAC1GLOB + tots[1]
        #EAC2GLOB = EAC2GLOB + tots[2]
        #idxvec = (idxvec + 1) %% 30
        #if(idxvec == 0)
        #{
        #  idxvec = 1
        #}
      }
    }
}
print('Historical Analysis Done')
