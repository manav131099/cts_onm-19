rm(list = ls())
library(stringr)
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN019CHistoricalNew.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/IN019CDigest/summaryFunctions_new.R')
RESETHISTORICAL=1
daysAlive = 0
reorderStnPaths = c(12,1,4,5,6,7,8,9,10,11,2,3)

if(RESETHISTORICAL)
{
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-019C]')
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-019C]')
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-019C]')
}

path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-019C]"
pathwrite2G = "/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-019C]"
pathwrite3G = "/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-019C]"
pathwrite4G = "/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-019C]"


checkdir(pathwrite2G)
checkdir(pathwrite3G)
checkdir(pathwrite4G)
years = dir(path)
for(x in 1 : length(years))
{
  if(as.numeric(years[x])<2020)
    next
  pathyr = paste(path,years[x],sep="/")
  pathwriteyr = paste(pathwrite2G,years[x],sep="/")
  checkdir(pathwriteyr)
  months = dir(pathyr)
  if(!length(months))
    next
  for(y in 1 : length(months))
  {
    if(years[x] == '2020')
      if(as.numeric(substr(months[y],6,7))<6)
        next
    pathmon = paste(pathyr,months[y],sep="/")
    pathwritemon = paste(pathwriteyr,months[y],sep="/")
    checkdir(pathwritemon)
    stns = dir(pathmon)
    if(!length(stns))
      next
    stns = stns[reorderStnPaths]
    
    for(z in 1 : length(stns))
    {
      type = 2
      pathstn = paste(pathmon,stns[z],sep="/")
      if(grepl("MFM",stns[z]))
        type = 0
      else if(grepl("Inverter",stns[z]))
        type = 1
      else
        next
      pathwritestn = paste(pathwritemon,stns[z],sep="/")
      checkdir(pathwritestn)
      days = dir(pathstn)
      if(!length(days))
        next
      for(t in 1 : length(days))
      {
        len = str_length(days[t])
        cur_date = substr(days[t],len-5,len-4)
        if(months[y] == '2020-06')
          if(cur_date < 13)
            next
        if(z == 1)
          daysAlive = daysAlive + 1
        pathread = paste(pathstn,days[t],sep="/")
        pathwritefile = paste(pathwritestn,days[t],sep="/")
        if(RESETHISTORICAL || !file.exists(pathwritefile))
        {
          secondGenData(pathread,pathwritefile,type)
          print(paste(days[t],"Done"))
        }
      }
    }
  }
}
# sink()