rm(list = ls(all = T))
source('/home/admin/CODE/common/math.R')
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
prepareSumm = function(dataread)
{
  
  daycall = da = daPerc = gs00 = smp = tamb =tamba= tambst = tambmx = tambmn = tambstmx = NA
  tambstmn = hamb = hambst = hambmx = hambmn = hambstmx = hambstmn = tse = tnw  = NA
  rowa0 = rowcj = trans = rowcjlast =rowa0last= timelast = ga = ga_ctr= ga_str = ssd=ssc=tsi2st=tsi1st=NA
	rowaq = rowcl = tmaq = tmcl = lastaq = lastcl = yldaq = yldcl = pr1ctr = pr2ctr = pr1str = pr2str = NA
  stryield=ctryield=stdev=cv=rowstr=timelastval_str=timelastval_ctr=timelast_str=timelast_ctr=rowst=rowct=gpyrrat=gpysirat=NA
  if(nrow(dataread)) 
  {
    print(paste('In if for',substr(dataread[1,1],1,10)))
    da = nrow(dataread)
		daPerc = da/14.4
    thresh = 20
    smp = sum(dataread[complete.cases(dataread[,3]),3])/60000
    gsi00 = sum(dataread[complete.cases(dataread[,4]),4])/60000
    subdata = dataread[complete.cases(dataread[,3]),]
    if(nrow(subdata))
      subdata = subdata[as.numeric(subdata[,3]) > thresh,]
    
    if(length(dataread[complete.cases(dataread[,11]),11]))
      tamb = mean(dataread[complete.cases(dataread[,11]),11])
    
    if(length(subdata[complete.cases(subdata[,11]),11]))
      tambst = mean(subdata[complete.cases(subdata[,11]),11])
    
    if(length(dataread[complete.cases(dataread[,9]),9]))
      hamb = mean(dataread[complete.cases(dataread[,9]),9])
    
    if(length(subdata[complete.cases(subdata[,9]),9]))
      hambst = mean(subdata[complete.cases(subdata[,9]),9])
    
    if(length(dataread[complete.cases(dataread[,11]),11]))
      tambmx = max(dataread[complete.cases(dataread[,11]),11])
    
    if(length(subdata[complete.cases(subdata[,11]),11]))
      tambstmx = max(subdata[complete.cases(subdata[,11]),11])
    
    if(length(dataread[complete.cases(dataread[,9]),9]))
      hambmx = max(dataread[complete.cases(dataread[,9]),9])
    
    if(length(subdata[complete.cases(subdata[,9]),9]))
      hambstmx = max(subdata[complete.cases(subdata[,9]),9])
    
    if(length(dataread[complete.cases(dataread[,11]),11]))
      tambmn = min(dataread[complete.cases(dataread[,11]),11])
    
    if(length(subdata[complete.cases(subdata[,11]),11]))
      tambstmn = min(subdata[complete.cases(subdata[,11]),11])
    
    if(length(dataread[complete.cases(dataread[,9]),9]))
      hambmn = min(dataread[complete.cases(dataread[,9]),9])
    
    if(length(subdata[complete.cases(subdata[,9]),9]))
      hambstmn = min(subdata[complete.cases(subdata[,9]),9])
    
    if(length(subdata[complete.cases(subdata[,12]),12]))
      tsi1st = mean(subdata[complete.cases(subdata[,12]),12])
    
    if(length(subdata[complete.cases(subdata[,13]),13]))
      tsi2st = mean(subdata[complete.cases(subdata[,13]),13])
    
    
    tnw=(sum(as.numeric(dataread[,5]))/60000)
    tse=(sum(as.numeric(dataread[,7]))/60000)
    n_row=nrow(dataread)
    s=n_row-length(which(dataread[,18]==0))
    ga=NA
    sd=length(which(dataread[,3]<=20))+length(which(dataread[,28]>=2255.5))
    
    cd=length(which(dataread[,3]<=20))+length(which(dataread[,75]>=2255.5))
    
    ssc=(sum(as.numeric(dataread[,6]))/60000)
    ssd=(sum(as.numeric(dataread[,8]))/60000)
    
    
    
    rowstr = dataread[,c(1,46,43)]
    rowstr = rowstr[complete.cases(rowstr[,c(2,3)]),]

    rowstr2 = dataread[,c(1,93,90)] #1,46,93,43,90
    rowstr2 = rowstr2[complete.cases(rowstr2[,c(2,3)]),]

    {
      if((nrow(rowstr)>0) || (nrow(rowstr2) > 0))
      {
        rowa0 = rowstr[(rowstr[,2] < 1000000000) & (rowstr[,2] > 0),]
				if(!nrow(rowa0))
        	rowa0 = data.frame(Tm=NA,C1=NA,C2=NA)
        
        rowcj = rowstr2[(rowstr2[,2] < 1000000000)& (rowstr2[,2] > 0),]
				if(!nrow(rowcj))
        	rowcj = data.frame(Tm=NA,C1=NA,C2=NA)

				rowaq = rowstr[(rowstr[,3] < 1000000000) & (rowstr[,3] > 0),]
				if(!nrow(rowaq))
        	rowaq = data.frame(Tm=NA,C1=NA,C2=NA)
				
				rowcl = rowstr2[(rowstr2[,3] < 1000000000) & (rowstr2[,3] > 0),]
				if(!nrow(rowcl))
        	rowcl = data.frame(Tm=NA,C1=NA,C2=NA)
        
				if(nrow(rowa0)>0)
        {
          rowa0last = rowa0[nrow(rowa0),2]
          rowst = round(abs(rowa0[1,2] - rowa0[nrow(rowa0),2]),2)
          timelastval_str=as.character(rowa0[nrow(rowa0),1])
        }
        else
        {
          rowst = NA
        }
        
        if(nrow(rowcj)>0)
        {
          rowcjlast = rowcj[nrow(rowcj),2]
          rowct = round(abs(rowcj[1,2] - rowcj[nrow(rowcj),2]),2)
          timelastval_ctr=as.character(rowcj[nrow(rowcj),1])
        }
        else
        {
          rowct = NA
        }
        if(nrow(rowaq)>0)
        {
          lastaq = rowaq[nrow(rowaq),3]
          tmaq=as.character(rowaq[nrow(rowaq),1])
					rowaq = round(abs(rowaq[1,3] - rowaq[nrow(rowaq),3]),2)
          yldaq = round(rowaq/2250.5,2)
        }
				else
				{
					rowaq = NA
				}
        if(nrow(rowcl)>0)
        {
          lastcl = rowcl[nrow(rowcl),3]
          tmcl = as.character(rowcl[nrow(rowcl),1])
					rowcl = round(abs(rowcl[1,3] - rowcl[nrow(rowcl),3]),2)
        	yldcl = round(rowcl/2250.5,2)
				}
        else
				{
					rowcl = NA
				}
      }
    }
    
    {
      if((dataread[nrow(dataread),46]<=0) | (is.na(dataread[nrow(dataread),46])) | (dataread[nrow(dataread),46]>1000000))
        timelast_str = NA
      else
        timelast_str = timelastval_str
      
      if((dataread[nrow(dataread),93]<=0) | (is.na(dataread[nrow(dataread),93])) | (dataread[nrow(dataread),93]>=1000000)) 
        timelast_ctr = NA
      else
        timelast_ctr = timelastval_ctr
    }
    
    
    stryield = rowst/2255.5
    ctryield = rowct/2255.5
    stdev=as.numeric(sdp(c(stryield,ctryield))) # Standard deviation Sample
    cv=as.numeric((stdev/as.numeric(mean(c(stryield,ctryield))))*100)   #Coefficient of varience*100 
    
    #####
    dp=subset(dataread,dataread[,21]>=7000 & dataread[,21]<=15000 & dataread[,3]>=20,select = c(2,21))
    sv=length(which(as.numeric(dataread[,3])>=20))
    slv=nrow(dp)-sum(is.na(dp[,2]))
    ga_str=(slv*100)/sv
    dp<-subset(dataread,dataread[,68]>=7000 & dataread[,68]<=15000 & dataread[,3]>=20,select = c(1,68))
    shv=nrow(dp)-sum(is.na(dp[,2]))
    ga_ctr=(shv*100)/sv
    
    
    trans = NA
    
    daycall =substr(dataread[1,1],1,10)
  }
  gpyrrat = round(smp/gsi00,3)
	gpysirat = round(smp/ssc,3)
	pr1ctr = ctryield*100/gsi00
	pr2ctr = (yldcl*100/gsi00)
	pr1str = stryield*100/gsi00
	pr2str = (yldaq*100/gsi00)

  datawrite = data.frame( Date = daycall, PtsRec = rf(da), Gsi = rf(gsi00),  SMP = rf(smp),
                          Tamb = rf1(tamb),  TambSH = rf1(tambst), TambMx = rf1(tambmx),  TambMn = rf1(tambmn),
                          TambSHmx = rf1(tambstmx),  TambSHmn = rf1(tambstmn),  Hamb = rf1(hamb), HambSH = rf1(hambst), 
                          Tamba=rf3(tamb), TambS=rf3(tambst),
                          HambMx = rf1(hambmx), HambMn = rf1(hambmn), HambSHmx = rf1(hambstmx), HambSHmn = rf1(hambstmn),
                          TModNW = rf1(tsi1st), TModSE= rf1(tsi2st),
                          TotEnergyDelSTR=rf(rowst), TotEnergyDelCTR=rf(rowct),
                          TransformerLoss = rf1(trans), LastReadingCTR=rf(rowcjlast),LastTimeSTR=(timelast_str),
                          grid_avl=rf1(ga), Grid_str_avl=rf1(ga_str), Grid_ctr_avl=rf1(ga_ctr), LastReadingSTR=rf(rowa0last), siClean=rf(ssc), 
                          sidirty=rf(ssd),GtiNW=rf(tnw),GtiSE=rf(tse),YieldSTR=rf(stryield),YieldCTR=rf(ctryield),
                          StdDev=rf(stdev),CoV=rf1(cv),LastTimeCTR=(timelast_ctr),LastTimevalSTR=(timelastval_str),
                          LastTimevalCTR=(timelastval_ctr),PyrGsiRat=gpyrrat,PyrCleanSiRat=gpysirat,
													Eac2Str = rowaq, Yld2Str=yldaq,LastReadStr=lastaq,LastTimeStr=tmaq,
													Eac2Ctr=rowcl,Yld2Ctr=yldcl,LastReadCtr=lastcl,LastTimeCtr=tmcl,DA=rf1(daPerc),
													PR1Ctr=rf1(pr1ctr),PR2Ctr=rf1(pr2ctr),PR1Str=rf1(pr1str),PR2Str=rf1(pr2str),
													stringsAsFactors=F)
  datawrite
}
# No need for Tamba and TambS; as already implemented with Tamb , TambSH
rewriteSumm = function(datawrite)
{
  
  df = data.frame(Date = as.character(datawrite[1,1]),Gsi = as.character(datawrite[1,3]),
	SMP = as.character(datawrite[1,4]),Tamb = as.character(datawrite[1,5]),
	TambSH = as.character(datawrite[1,6]),Hamb = as.character(datawrite[1,11]),
	HambSH = as.character(datawrite[,12]),TotEnergyDelSTR = as.character(datawrite[1,21]),
	TotEnergyDelCTR=as.character(datawrite[1,22]),TransformerLoss=as.character(datawrite[1,23]),
	GsiClean=datawrite[1,30],GsiDirty=datawrite[1,31],LastReadingCTR=datawrite[1,24],LastReadingSTR=datawrite[1,29]
  ,Eac2Str=datawrite[1,43],Eac2Ctr=datawrite[1,47],LastTimeStr=datawrite[1,46],stringsAsFactors=F)
  df
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[718]"
pathwrite = "/home/admin/Dropbox/Second Gen/[IN-036S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    days = days[grepl("txt",days)]
    sumfilename = paste("[IN-036S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread)
      datasum = rewriteSumm(datawrite)
      daywr = gsub("718","IN-036S",days[z])
      write.table(datawrite,file = paste(writemonth,daywr,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
