source("/home/admin/CODE/common/aggregate.R")
require('openssl')
options(warn=-1)

uniqIDGen = function(stn_code, mt_code)
{
	val = paste(stn_code,mt_code,sep="_")
	val = as.numeric(paste("0x",substr(as.character(md5(val)),1,7),sep=""))
	return(val)
}

sendAggregateInfo = function(date,pathWrite = "/home/admin/Dropbox/HQDigest/MailDigest")
{
  path = "/home/admin/Start/Aggregate"
	colnamesFile = getNameTemplate()
	numericalExceptions = fetchNumericalExceptions()
	extraInfo = c("STN_ID","Meter_ID","Type","Country","UniqID")
	colnamesFile = c(extraInfo,colnamesFile)
	
	files = dir(path)
	meters = files[grepl("_Meters.txt",files)]

	yr = substr(date,1,4)
	mon = substr(date,1,7)

	for(x in 1 : length(meters))
	{
		stnName = unlist(strsplit(meters[x],"_"))[1]
		stnType = substr(stnName,nchar(stnName),nchar(stnName))
		provider = NA
		{
			if(stnType == "X" || stnType == "S" || stnType == "L" || stnType == "W")
			{
				basePath = "/home/admin/Dropbox/Second Gen"
				provider = "EBX"
				{
					if(stnType == "S")
						provider = "SERIS"
					else if(stnType == "L")
						provider = "Locus"
					else if(stnType == "W")
						provider = "Webdyn"
				}
			}
			else if(stnType == "C")
			{
				provider = "Flexi"
				basePath = "/home/admin/Dropbox/FlexiMC_Data/Second_Gen"
			}
		}
		stnPath = paste(basePath,"/[",stnName,"]",sep="")
		pathDays = paste(stnPath,yr,mon,sep="/")
		pathMeter = paste(path,meters[x],sep="/")
		metersData = readLines(pathMeter)
		meterSubFolder = 0
		if(length(metersData) > 1)
		{
			idx = match("InSameFile",metersData)
			{
			if(!is.finite(idx))
			{
				meterSubFolder = 1
			}
			else
			{
				metersData = metersData[-idx]
			}
			}
		}
		for( y in 1 : length(metersData))
		{
			rowTemplate = unlist(rep(NA,length(colnamesFile)))
			rowTemplate[1] = stnName
			rowTemplate[2] = metersData[y]
			if(metersData[y] == "")
				rowTemplate[2] = NA
			rowTemplate[3] = provider
			rowTemplate[4] = substr(stnName,1,2)
			rowTemplate[5] = uniqIDGen(rowTemplate[1],rowTemplate[2])
			pathCol = paste(path,"/",stnName,"-",metersData[y],"_Cols.txt",sep="")
			if(metersData[y] == "")
			{
				pathCol = paste(path,"/",stnName,"_Cols.txt",sep="")
			}
			colInfo = read.table(pathCol,header=T,sep="\t",stringsAsFactors=F)
			pathDaysFinal = pathDays
			if(meterSubFolder)
				pathDaysFinal = paste(pathDays,metersData[y],sep="/")
			if(stnName=='IN-007C')
			{
				pathDaysFinal = paste(pathDays,metersData[y],sep="/")
			}
			days2G = dir(pathDaysFinal)
			day2Gfinal = days2G[grepl(date,days2G)]
			if(length(day2Gfinal) == 1)
			{
				data2G = read.table(paste(pathDaysFinal,day2Gfinal,sep="/"),header=T,sep="\t",stringsAsFactors=F)
				for(z in 1 : nrow(colInfo))
				{
					{
						if(z != numericalExceptions && is.finite(as.numeric(colInfo[z,2])))
							rowTemplate[length(extraInfo)+z] = as.character(data2G[1,as.numeric(colInfo[z,2])])
						else if(z == numericalExceptions)
							rowTemplate[length(extraInfo)+z] = as.character(colInfo[z,2])
					}
				}
			}
		{
			if(x == 1 && y ==1)
				dataHQ = rowTemplate
			else
				dataHQ = rbind(dataHQ,rowTemplate)
		}
	}
}
dataHQ = data.frame(dataHQ,stringsAsFactors=F)
colnames(dataHQ) = colnamesFile
filepath = paste(pathWrite,"/HQ_",date,".txt",sep="")
write.table(dataHQ,file=filepath,sep="\t",row.names=F,col.names=T,append=F)
}

