rm(list = ls())
errHandle = file('/home/admin/Logs/LogsPH002History.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/PH002Digest/FTPPH002Dump.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
FIREERRATA = c(1)
LTCUTOFF = 0.001
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins))+1))
}
FIRETWILIONA =c(0)
THRESHOLDLOCK = 10
COUNTERLOCK = 0
checkErrata = function(row,ts)
{
  if(ts < 540 || ts > 1020)
	{return()}
	if(FIREERRATA == 0)
	{
	  print(paste('Errata mail already sent for meter so no more mails'))
		return()
	}
	{
	if(!is.finite(as.numeric(row[2])))
	{
		FIRETWILIONA <<- FIRETWILIONA + 1
	}
	else
	{
		FIRETWILIONA <<- 0
		{
			if((as.numeric(row[2]) < LTCUTOFF))
				COUNTERLOCK <<- COUNTERLOCK + 1
			else
				COUNTERLOCK <<- 0
		}
	}
	}
	if((FIRETWILIONA > 10) || ((COUNTERLOCK >= THRESHOLDLOCK) && (is.finite(as.numeric(row[2])))))
	{
		row[2] = as.numeric(row[2])/1000
		FIREERRATA <<- 0
		subj = paste('PH-002X down')
    body = 'Last recorded timestamp and reading\n'
		body = paste(body,'\nTimestamp:',as.character(row[1]))
		body = paste(body,'\n\nReal Power Tot kW reading:',as.character(round(as.numeric(row[2]),3)))
		if(FIRETWILIONA >10)
		{
		body = paste(body,'\n\nMore than 10 continuous readings are NA')
		}
		rec=getRecipients("PH-002X","a")
		send.mail(from = 'operations@cleantechsolar.com',
							to = rec,
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F)
		print(paste('Errata mail sent meter '))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
		                "\n Real Pow Tot kW reading:",as.character(row[2]))
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "PH-002X"',sep = "")
		system(command)
		print('Twilio message fired')
		recordTimeMaster("PH-002X","TwilioAlert",as.character(row[1]))
		FIRETWILIONA <<-0
		COUNTERLOCK <<- 0
	}
}

stitchFile = function(path,days,pathwrite,erratacheck)
{
x = 1
t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
        {
                print(paste("Skipping",days[x],"Due to err in file"))
                return()
        }
  dataread = t
        if(nrow(dataread) < 1)
                return()
  dataread = dataread[,-c(1,2)]
	tmstmps = as.character(dataread[,1])
	tmstmps = unlist(strsplit(tmstmps," "))
  allseqs = seq(from = 1, to = length(tmstmps),by=2)
	tmstmps2 = tmstmps[allseqs]
	tmstmps2 = unlist(strsplit(tmstmps2,"-"))
	allseqa = seq(from = 1, to = length(tmstmps2),by=3)
	tmstmps2 = paste(tmstmps2[(allseqa+2)],tmstmps2[allseqa],tmstmps2[(allseqa+1)],sep="-")
	tmstmps = paste(tmstmps2,tmstmps[(allseqs+1)])
	dataread[,1] = tmstmps
	{
	if(grepl("Gsi00",days[x]))
	{
		idxvals = length(colnames) - 2
	}
	else if(grepl("Tmod",days[x]))
	{
		idxvals = length(colnames) -1
	}
	else if(grepl("Export",days[x]))
	{
		idxvals = length(colnames)
	}
	else
	{
	currcolnames = colnames(dataread)
  idxvals = match(currcolnames,colnames)
  }
	}
	temp = unlist(strsplit(days[x],"_"))
	print(paste('temp is',temp[2]))
  temp=1
  for(y in 1 : nrow(dataread))
  {
    idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
    rowtemp2 = rowtemp
    yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
		pathwritefinal = pathwritemon
    checkdir(pathwritefinal)
    mtno = temp
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname,day,".txt",sep="")
    pathtowrite = paste(pathwritefinal,filename,sep="/")
		{
		if(grepl("Gsi00",days[x]) || grepl("Tmod",days[x]) || grepl("Export",days[x]))
		{
		print(paste('Updating row index',idxvals))
		rowtemp2[idxvals] = as.numeric(dataread[y,2])
		rowtemp2[1] = as.character(dataread[y,1])
		}
		else
		rowtemp2[idxvals] = dataread[y,]
		}
		{
    if(!file.exists(pathtowrite))
    {
			rowtemp2 = rbind(unlist(rowtemp2))
      df = data.frame(rowtemp2,stringsAsFactors = F)
      #if(idxts != 1)
      #  {
       #   df[idxts,] = rowtemp2
       #   df[1,] = rowtemp
       # }
			print(paste('length(colnames)',length(colnames)))
			print(paste('length(rowtemp2)',length(rowtemp2)))
			print(paste('ncols(df)',ncol(df)))
      colnames(df) = colnames
			print('Assigned')
			FIREERRATA <<- c(1)
			FIRETWILIONA <<- c(0)
			COUNTERLOCK <<- 0
    }
    else
    {
      df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
      rowtemp2 = unlist(rowtemp2)
			{
			if(idxts > nrow(df))
			{
				df[idxts,]=rowtemp2
			}
			else
			{
			if(as.character(rowtemp2[1])==as.character(df[idxts,1]))
			{
			if(grepl("Gsi00",days[x]) || grepl("Tmod",days[x]) || grepl("Export",days[x]))
				df[idxts,idxvals]=dataread[y,2]#dont replace with rowtemp2 this is correct
			else
				df[idxts,(1:(length(rowtemp2)-3))] = rowtemp2[1:(length(rowtemp2)-3)]
			}
			else
			{
				print(paste('Mismatch Old time',as.character(df[idxts,1]),
				'New time',as.character(rowtemp2[1])))
				df[(idxts+1):(nrow(df)+1),] = df[idxts:nrow(df),]
				df[idxts,]=rowtemp2
			}
			}
			}
    }
  }
	if(erratacheck != 0 && (!(grepl('Gsi00',days[x]) || grepl("Tmod",days[x]) || grepl("Export",days[x]))))
	{
	pass = c(as.character(df[idxts,1]),as.character(df[idxts,15]))
	checkErrata(pass,idxts)
	}
	print(days)
	idxtouse = as.character(df[,1])
	idxtouse = idxtouse[complete.cases(idxtouse)]
	idxtouse = match(idxtouse,as.character(df[,1]))
	#print(idxtouse)
	df = df[idxtouse,]
	if(nrow(df))
	{
	tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
	df = df[order(tdx),]
  write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F)
  }
	}
	recordTimeMaster("PH-002X","Gen1",as.character(df[nrow(df),1]))
}

path = '/home/admin/Data/Episcript-CEC/EGX PH002 Dump'
checkdir(path)
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[PH-002X]'
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'PH002.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
lastrecordeddate = c('','','')
idxtostart = c(1,1,1,1)
{
	if(!file.exists(pathdatelastread))
	{
		print('Last date read file not found so cleansing system from start')
#		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[PH-002X]"')
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		#lastrecordeddate = c(lastrecordeddate[1])
		days1 = days[grepl('PH-002',days)]
		days2 = days[grepl('Gsi00',days)]
		days3 = days[grepl('Tmod',days)]
		days4 = days[grepl('Export',days)]

		idxtostart[1] = match(lastrecordeddate[1],days1)
		idxtostart[2] = match(lastrecordeddate[2],days2)
		if(length(lastrecordeddate)>2)
		idxtostart[3] = match(lastrecordeddate[3],days3)
		idxtostart[4] = match(lastrecordeddate[4],days4)
		print(paste('Date read is',lastrecordeddate,'and idxtostart is',idxtostart[1]))
	}
}
checkdir(pathwrite)
idxtomatchph = match(TRUE,grepl("PH-002_20170103200000",days))
colnames = colnames(read.csv(paste(path,days[idxtomatchph],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames = colnames[-c(1,2)]
colnames = c(colnames,"Gsi00","Tmod","ActiveEnergy")
rowtemp = unlist(rep(NA,(length(colnames))))
x=1
stnname =  "[PH-002X] "
days1 = days[grepl('PH-002',days)]
days2 = days[grepl('Gsi00',days)]
days3 = days[grepl('Tmod',days)]
days4 = days[grepl('Export',days)]
print(paste('length of days1',length(days1)))
print(paste('length of days2',length(days2)))
print(paste('length of days3',length(days3)))
print(paste('length of days4',length(days4)))

{
	if(idxtostart[1] <= length(days1))
	{
		print(paste('idxtostart[1] is',idxtostart[1],'while length of days1 is',length(days1)))
		for(x in idxtostart[1] : length(days1))
		{
 		 stitchFile(path,days1[x],pathwrite,0)
		}
		lastrecordeddate[1] = as.character(days1[x])
		print('Meter 1 DONE')
	}
	if(idxtostart[2] <= length(days2))
	{
		print(paste('idxtostart[2] is',idxtostart[2],'while length of days2 is',length(days2)))
		for(x in idxtostart[2] : length(days2))
		{
 		 stitchFile(path,days2[x],pathwrite,0)
		}
		lastrecordeddate[2] = as.character(days2[x])
		print('Meter 2 DONE')
	}
	if(idxtostart[3] <= length(days3) && length(days3))
	{
		print(paste('idxtostart[3] is',idxtostart[3],'while length of days3 is',length(days3)))
		for(x in idxtostart[3] : length(days3))
		{
 		 stitchFile(path,days3[x],pathwrite,0)
		}
		lastrecordeddate[3] = as.character(days3[x])
		print('Meter 2 DONE')
	}
	if(idxtostart[4] <= length(days4) && length(days4))
	{
		print(paste('idxtostart[4] is',idxtostart[4],'while length of days3 is',length(days4)))
		for(x in idxtostart[4] : length(days4))
		{
 		 stitchFile(path,days4[x],pathwrite,0)
		}
		lastrecordeddate[4] = as.character(days4[x])
		print('Meter 2 DONE')
	}
	else if( (!(idxtostart[1] < length(days1))) && (!(idxtostart[2] < length(days2))))
	{
		print('No historical backlog')
	}
}

print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
print(lastrecordeddate)
write(lastrecordeddate,pathdatelastread)
print('File Written')
FIRSTTIMETRIGGER=0
while(1)
{
  print('Checking FTP for new data')
  filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(600)
		next
	}
	print('FTP Check complete')
	daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	if(length(daysnew) == length(days))
	{
	  if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		Sys.sleep(600)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	daysnew = daysnew[-match]
	for(x in 1 : length(daysnew))
	{
	  print(paste('Processing',daysnew[x]))
		stitchFile(path,daysnew[x],pathwrite,FIRSTTIMETRIGGER)
		print(paste('Done',daysnew[x]))
	}
	daysnew2 = daysnew[grepl('Gsi00',daysnew)]
	daysnew3 = daysnew[grepl('Tmod',daysnew)]
	daysnew4 = daysnew[grepl('Export',daysnew)]
	daysnew = daysnew[grepl('PH-002',daysnew)]
	if(length(daysnew))
	lastrecordeddate[1] = as.character(daysnew[length(daysnew)])
	if(length(daysnew2))
	lastrecordeddate[2] = as.character(daysnew2[length(daysnew2)])
	if(length(daysnew3))
	lastrecordeddate[3] = as.character(daysnew3[length(daysnew3)])
	if(length(daysnew4))
	lastrecordeddate[4] = as.character(daysnew4[length(daysnew4)])

	write(c(lastrecordeddate[1],lastrecordeddate[2],lastrecordeddate[3],lastrecordeddate[4]),pathdatelastread)
	FIRSTTIMETRIGGER=1
gc()
}
print('Out of loop')
sink()
