rm(list = ls())
source('/home/admin/CODE/common/math.R')
TIMESTAMPSALARM = NULL
ltcutoff = .001
CABLOSSTOPRINT = 0
INSTCAP=c(53.76)
TOTSOLAR = 0
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

fetchGSIData = function(date)
{
	pathMain = '/home/admin/Dropbox/Second Gen/[VN-001S]'
	yr = substr(date,1,4)
	mon = substr(date,1,7)
	txtFileName = paste('[VN-001S] ',date,".txt",sep="")
	pathyr = paste(pathMain,yr,sep="/")
	gsiVal = NA
	if(file.exists(pathyr))
	{
		pathmon = paste(pathyr,mon,sep="/")
		if(file.exists(pathmon))
		{
			pathfile = paste(pathmon,txtFileName,sep="/")
			if(file.exists(pathfile))
			{
				dataread = read.table(pathfile,sep="\t",header = T)
				gsiVal = as.numeric(dataread[,4])
			}
		}
	}
	return(c(gsiVal))
}

secondGenData = function(filepath,writefilepath)
{
	 TIMESTAMPSALARM <<- NULL
	
	print(paste('IN 2G',filepath))
  dataread = try(read.table(filepath,header = T,sep = "\t",stringsAsFactors=F),silent=T)
	date = DA = NA 
	Eac1I1 =  Eac2I1 = NA 
	Eac1I2 = Eac2I2 = NA
	Eac1I3 = Eac2I3 = NA
	Eac1I4 = Eac2I4 = NA
	Eac1I5 = Eac2I5 = NA
	Eac1I6 = Eac2I6 = NA
	PR = Yld = Yld1 = Yld2 = Yld3 = Yld4 = Yld5 = Yld6 = NA
	SolAct=GridAct=SolPen=NA
	Gpy=Yld1 = Yld2 = PR1 = PR2 = totrowsmissing = NA 
	lasttime = lastread = NA
	Gsi = SdYld = CovYld = NA
	
	if(class(dataread) == "try-error")
	{
      df = data.frame(Date = date,
									DA = DA,
									Downtime = totrowsmissing,
									Eac1I1=Eac1I1, 
									Eac1I2=Eac1I2,
									Eac1I3=Eac1I3, 
									Eac1I4=Eac1I4, 
									Eac1I5=Eac1I5, 
									Eac1I6=Eac1I6, 
									SolAct=SolAct,
									GridAct=GridAct,
									SolPen=SolPen,
									Yld = Yld,
									GSi = Gsi,
									PR = PR,
									YldInv1 = Yld1,
									YldInv2 = Yld2,
									YldInv3 = Yld3,
									YldInv4 = Yld4,
									YldInv5 = Yld5,
									YldInv6 = Yld6,
									SdYld = SdYld,
									CovYld = CovYld,
									stringsAsFactors = F)
		write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
		return()
	}

	dataread2 = dataread
	colnoUse = 39
	
	idxpac = 4
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	if(nrow(dataread) > 1)
	{
   	Eac1I1 = format(round(sum(as.numeric(dataread[,idxpac]))/60,1),nsmall=2)
	}
	Yld1 = round(as.numeric(Eac1I1) / 71.5,2)
	idxpac = 12
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	if(nrow(dataread) > 1)
	{
   	Eac1I2 = format(round(sum(as.numeric(dataread[,idxpac]))/60,1),nsmall=2)
	}
	Yld2 = round(as.numeric(Eac1I2) / 71.5,2)
	
	idxpac = 9
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	if(nrow(dataread) > 1)
	{
   	Eac1I3 = format(round(sum(as.numeric(dataread[,idxpac]))/60,1),nsmall=2)
	}
	Yld3 = round(as.numeric(Eac1I3) / 71.5,2)
	
	idxpac = 6
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	if(nrow(dataread) > 1)
	{
   	Eac1I4 = format(round(sum(as.numeric(dataread[,idxpac]))/60,1),nsmall=2)
	}
	Yld4 = round(as.numeric(Eac1I4) / 71.5,2)
	
	idxpac = 2
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	if(nrow(dataread) > 1)
	{
   	Eac1I5 = format(round(sum(as.numeric(dataread[,idxpac]))/60,1),nsmall=2)
	}
	Yld5 = round(as.numeric(Eac1I5) / 71.5,2)
	
	idxpac = 10
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	if(nrow(dataread) > 1)
	{
   	Eac1I6 = format(round(sum(as.numeric(dataread[,idxpac]))/60,1),nsmall=2)
	}
	Yld6 = round(as.numeric(Eac1I6) / 65,2)
	
	idxpac = 7
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	if(nrow(dataread) > 1)
	{
   	SolAct = format(round(sum(as.numeric(dataread[,idxpac]))/60,1),nsmall=2)
	}
	Yld = round(as.numeric(SolAct) / 422.5,2)
	
	idxpac = 8
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
	dataread = dataread[as.numeric(dataread[,idxpac]) > 0,]
	if(nrow(dataread) > 1)
	{
   	GridAct = format(round(sum(as.numeric(dataread[,idxpac]))/60,1),nsmall=2)
	}
	SolPen = round(as.numeric(SolAct)*100/(as.numeric(GridAct) +as.numeric(SolAct)),1)
	dataread = dataread2
	datareadcp = dataread2
  DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 480,]
  tdx = tdx[tdx > 480]
  dataread2 = dataread2[tdx < 1020,]
	idxpac = 7
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
  missingfactor = 540 - nrow(dataread2)
  dataread2 = dataread2[as.numeric(dataread2[,idxpac]) < ltcutoff,]
	if(length(dataread2[,1]) > 0)
	{
		TIMESTAMPSALARM <<- as.character(dataread2[,1])
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/5.4,1),nsmall=1)
	date = NA
	if(nrow(dataread)>0)
		date = substr(as.character(dataread[1,1]),1,10)
	Gsi = fetchGSIData(date)
	PR = round(as.numeric(Yld)*100 / Gsi,1)
	SdYld = round(sdp(c(Yld1,Yld2,Yld3,Yld4,Yld5,Yld6)),3)
	CovYld = round(SdYld * 100/mean(c(Yld1,Yld2,Yld3,Yld4,Yld5,Yld6)),1)
  df = data.frame(Date = date,
							DA = DA,
							Downtime = totrowsmissing,
							Eac1I1=Eac1I1, 
							Eac1I2=Eac1I2,
							Eac1I3=Eac1I3, 
							Eac1I4=Eac1I4, 
							Eac1I5=Eac1I5, 
							Eac1I6=Eac1I6, 
							SolAct=SolAct,
							GridAct=GridAct,
							SolPen=SolPen,
							Yld = Yld,
							GSi = Gsi,
							PR = PR,
							YldInv1 = Yld1,
							YldInv2 = Yld2,
							YldInv3 = Yld3,
							YldInv4 = Yld4,
							YldInv5 = Yld5,
							YldInv6 = Yld6,
							SdYld = SdYld,
							CovYld = CovYld,
							stringsAsFactors = F)

	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1,writefilepath)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F) #its correct dont change
  {
    if(file.exists(writefilepath))
    {
      write.table(dataread1,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(dataread1,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}

